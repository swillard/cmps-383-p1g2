﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phase1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "CMPS 383 Phase 1 Group 2";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "CMPS 383 Phase 1 Group 2";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "CMPS 383 Phase 1 Group 2";

            return View();
        }
    }
}
