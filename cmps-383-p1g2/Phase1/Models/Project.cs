﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Phase1.Models
{
    public class Project
    {
        [Key]
        [Required]
        public int ID { get; set; }
        [Required]
        public String Name { get; set; }
        
        public User AssignedTo { get; set; } //Needs to be dropdown list
        //public virtual IList<User> AssignedTo { get; set; }
        public int UserId { get; set; }
        [Required]
        public string RepositoryLocation { get; set; } // NEEDS TO BE TEXT BOX
    }
}